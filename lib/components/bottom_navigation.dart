import 'package:flutter/material.dart';
import 'package:receipe_list/view/food_dessert.dart';
import 'package:receipe_list/view/food_seafood.dart';

class BottomNavigationController extends StatefulWidget {
  @override
  _BottomNavigationControllerState createState() =>
      _BottomNavigationControllerState();
}

class _BottomNavigationControllerState
    extends State<BottomNavigationController> {
  int currentTapIndex = 0;
  final List<Widget> pages = [
    FoodSeafood(
      key: PageStorageKey('Page1'),
    ),
    FoodDessert(
      key: PageStorageKey('Page2'),
    )
  ];
  final PageStorageBucket bucket = PageStorageBucket();
  Widget bottomNavigation(int index) => BottomNavigationBar(
        onTap: (int index) => setState(() => currentTapIndex = index),
        currentIndex: index,
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.local_dining),
            title: new Text('Seafood'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.cake),
            title: new Text('Dessert'),
          ),
        ],
      );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: bottomNavigation(currentTapIndex),
      body: PageStorage(
        child: pages[currentTapIndex],
        bucket: bucket,
      ),
    );
  }
}
