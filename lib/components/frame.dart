import 'package:flutter/material.dart';

class Frame extends StatefulWidget {
  Frame({this.body, this.title});
  final Widget body;
  final String title;
  @override
  _FrameState createState() => _FrameState();
}
class _FrameState extends State<Frame> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: widget.body
    );
  }
}
