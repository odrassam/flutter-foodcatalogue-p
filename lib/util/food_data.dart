import 'networking.dart';

String url = 'https://themealdb.com/api/json/v1/1/filter.php?c=';
String urlDesc = 'https://themealdb.com/api/json/v1/1/lookup.php?i=';
class GetFoodData{
  NetworkHelper2 networkHelper2;
  Future getData(String categories) async {
    networkHelper2 = NetworkHelper2(url: '$url$categories');
    var response = await networkHelper2.getData();
    return response['meals'];
  }
  Future getDataDescription(String id) async{
    networkHelper2 = NetworkHelper2(url: '$urlDesc$id');
    var response = await networkHelper2.getData();
    return response['meals'];
  }
}
