import 'package:flutter/material.dart';
import './view/food_seafood.dart';
import './view/food_dessert.dart';
import './components/bottom_navigation.dart';

void main() => runApp(MainComponent());

class MainComponent extends StatefulWidget {
  @override
  _MainComponentState createState() => _MainComponentState();
}

class _MainComponentState extends State<MainComponent> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BottomNavigationController(),
    );
  }
}





