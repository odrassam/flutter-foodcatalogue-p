import 'package:flutter/material.dart';
import 'package:receipe_list/components/reuseable_card.dart';
import '../util/food_data.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../components/frame.dart';

class FoodDessert extends StatefulWidget {
  FoodDessert({Key key}) : super(key: key);
  static const String id = 'Food_Dessert';
  @override
  _FoodDessertState createState() => _FoodDessertState();
}

class _FoodDessertState extends State<FoodDessert> {
  GetFoodData foodData = GetFoodData();
  Frame frame;
  var data = [];
  getFood(String category) async {
    var response = await foodData.getData(category);
    setState(() {
      data = response;
    });
  }

  renderFood() {
    if (data.length == 0) {
      return Center(
        child: SpinKitDoubleBounce(
          color: Colors.lightBlueAccent,
        ),
      );
    } else {
      List<Widget> foodCard = [];
      for (var x in data) {
        foodCard.add(
          ReuseableCard(
            image: x['strMealThumb'],
            title: x['strMeal'],
            index: x['idMeal'],
          ),
        );
      }
      return GridView.count(crossAxisCount: 2, children: foodCard);
    }
  }

  @override
  void initState() {
    super.initState();
    getFood('dessert');
  }

  @override
  Widget build(BuildContext context) {
    return frame = Frame(
      body: renderFood(),
      title: 'Dessert',
    );
  }
}
