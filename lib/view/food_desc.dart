import 'package:flutter/material.dart';
import '../util/food_data.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class FoodDesc extends StatefulWidget {
  FoodDesc({this.image, this.index, this.title});
  final image;
  final index;
  final title;
  @override
  _FoodDescState createState() => _FoodDescState();
}

class _FoodDescState extends State<FoodDesc> {
  GetFoodData getFoodData = GetFoodData();
  getFutureWidget(String id, String title, String value) {
    return FutureBuilder(
      future: getFoodData.getDataDescription(id),
      builder: (BuildContext context, AsyncSnapshot data) {
        if (data.hasData) {
          List<Widget> text = [
            Text('List Bahan Makanan : '),
            SizedBox(
              height: 10.0,
            )
          ];
          for(var i = 1; i <= 20; i++){
            if(data.data[0]['strIngredient$i'].length != 0){
              print(data.data[0]['strIngredient$i']);
              text.add(
                Text(
                  data.data[0]['strIngredient$i'],
                )
              );
            }
          }
          return Card(
            elevation: 2.0,
            margin: EdgeInsets.only(bottom: 20.0),
            color: Colors.grey.shade200,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: text,
              )
            ),
          );
        } else {
          return Center(
            child: SpinKitDoubleBounce(
              color: Colors.lightBlueAccent,
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Food Description',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: ListView(
          padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 10.0),
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Hero(
                  tag: '${widget.index}',
                  child: Container(
                    margin: EdgeInsets.only(bottom: 20.0),
                    height: 100.0,
                    width: 100.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(widget.image),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    widget.title,
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  height: 30.0,
                  width: 150.0,
                  child: Divider(color: Colors.black),
                ),
                getFutureWidget(
                    widget.index, 'Bahan Makanan :', 'strInstructions'),
              ],
            )
          ],
        ));
  }
}
